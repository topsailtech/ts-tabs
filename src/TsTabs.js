import {default as template} from './TsTabs-template.js'

export class TsTabs extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({mode: 'open'}).innerHTML = template
  }

  connectedCallback() {
    this._tabsSlot = this.shadowRoot.querySelector("slot[name=tab_tip]")
    this._panelsSlot = this.shadowRoot.querySelector('slot:not([name])')

    this._tabsSlot.addEventListener('click', tabTipClick.bind(this))

    setTimeout(()=>{
      dispatchEventsAndActivate.bind(this)(this.active || 1, false)
    }, 5)
  }

  get active(){
    return Number(this.getAttribute("active")) || null
  }

  /* according to recommendation, don't trigger events when invoking via javascript */
  set active(idx){
    toggleTab.bind(this)(this.active, false)
    toggleTab.bind(this)(idx, true)
    this.setAttribute("active", idx)
  }

  tabPanel(idx){
    return this.tabPanels[idx - 1]
  }

  tabTip(idx){
    return tabTips.bind(this)()[idx - 1]
  }

  get tabPanels(){
    return this._panelsSlot.
      assignedNodes({ flatten: true }).
      filter(el => el.nodeType === Node.ELEMENT_NODE)
  }
}

function toggleTab(idx, force){
  if (!idx){ return }

  const tip = this.tabTip(idx),
        panel = this.tabPanel(idx)

  if (tip){
    tip.toggleAttribute('aria-selected', force)
    tip.toggleAttribute('active', force)
  }
  if (panel){
    panel.toggleAttribute('aria-hidden', !force)
    panel.toggleAttribute('active', force)
  }
}

function tabTips(){
  return this._tabsSlot.assignedNodes({ flatten: true })
}

function tabTipClick(evt){
  const clickedTab = evt.target.closest('[slot="tab_tip"]'),
        newActive = tabTips.bind(this)().indexOf(clickedTab) + 1
  if (this.active != newActive){
    dispatchEventsAndActivate.bind(this)(newActive)
  }
}

function dispatchEventsAndActivate(idx, deactivating=true){
  let evt_detail = { old: (deactivating ? this.active : null), new: idx },
      beforeActivate = new CustomEvent("beforeActivate", { bubbles: true, cancelable: true, detail: evt_detail }),
      allowed = this.dispatchEvent(beforeActivate) || !deactivating
  if (allowed) {
    let activate = new CustomEvent(
                         "activate",
                         { bubbles: true, cancelable: false, detail: evt_detail }
                       )
    this.active = idx
    this.dispatchEvent(activate)
  }
}
