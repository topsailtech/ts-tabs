# \<ts-tabs>

## Usage
```html
<script type="importmap">
  {
    "imports": {
      "ts-tabs": "https://unpkg.com/@topsail/ts-tabs"
    }
  }
</script>
<script type="module">
  import 'ts-tabs';
</script>
<link rel="stylesheet" href="https://unpkg.com/@topsail/ts-tabs/themes.css"/><!-- just if you want to use the gray_theme -->
```

Add `slot="tab_tip"` and `section` elements side by side
```html
<ts-tabs active="2" class="gray_theme">
    <span slot="tab_tip">Tab 1</span>
    <section>Tab 1 Content</section>

    <button slot="tab_tip">Tab 2</button>
    <section>Tab 2 Content</section>
</ts-tabs>
```

or group tab tips first and then all the `section`s
```html
<ts-tabs>
    <span slot="tab_tip">Tab 1</span>
    <button slot="tab_tip">Tab 2</button>

    <section>Tab 1 Content</section>
    <section>Tab 2 Content</section>
</ts-tabs>
```

To locate the tab tips on the side, add attribute `side` to the `ts-tabs` root element.

To start with 2nd tab as active, add `active=2` to the `ts-tabs` tag.

### Methods
- Getter `active` for active tab index (1-based; null if none active)
- Setter `active=` for new active tab index (1-based), without triggering events
- Getter `tabPanels` the DOM elements for the tab panels (not the tab tips)
- `tabPanel(idx)` returns panel at position idx (1-based)
- `tabTip(idx)` returns tab tip at position idx (1-based)


### Events

**Events are not trigged if tabs are set progarmatically via `active=`.**

Both events have event.details which is an Object with keys:
- `old` the tab idx of the deactivating tab; null at initial activation
- `new` the tab idx of the newly activating tab

#### `beforeActivate(CustomEvent)`
triggered immediately before a tab is activated.

The event can be canceled to prevent the tab from activating, except when the initial tab is about to be shown.

#### `activate(CustomEvent)`
triggered after a tab has been activated.

### Styling
just add class `gray_theme`, or see `.gray_theme` section in CSS for example.


## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.


## Local Demo with `web-dev-server`

```bash
npm install
npm start
```

To run a local development server that serves the basic demo located in `demo/index.html`


## Publish to NPM

```bash
npm login
npm run deploy
```