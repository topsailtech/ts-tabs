export default `
<style>
:host {
  display: inline-block;
}

/* using :not([active]) rather than [aria-hidden],
  since "activation" can happen before all panels are slotted */
:host > #container > #panels > ::slotted(section:not([active])) {
  display: none;
}

:host > #container > #tab_tips {
  display: inline-flex;
  -webkit-user-select: none;
  user-select: none;
}
:host > #container > #tab_tips > ::slotted(:not([aria-selected])) {
  cursor: pointer;
}

/* side tabs */
:host([side]) > #container { display: flex; }
:host([side]) > #container > #tab_tips { display: block; }
:host([side]) > #container > #tab_tips > ::slotted(*) { display: block; }
:host([side]) > #container > #panels { flex: 1; }
</style>

<div id="container">
<div id="tab_tips">
  <slot name="tab_tip"></slot>
</div>
<div id="panels">
  <slot></slot>
</div>
</div>
`